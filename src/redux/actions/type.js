export const SET_ALERT = 'SET_ALERT'
export const REMOVE_ALERT = 'REMOVE_ALERT'
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
export const REGISTER_FAILURE = 'REGISTER_FAILURE'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILURE = 'LOGIN_FAILURE'
export const USER_LOADED = 'USER_LOADED'
export const AUTH_ERROR ='AUTH_ERROR'
export const LOGOUT = 'LOGOUT'
export const CLEAR_PROFILE = 'CLEAR_PROFILE';
export const API_DONE = 'API_DONE'
export const API_FAILURE='API_FAILURE'
export const SHOW_ALERT = "SHOW_ALERT"
export const REDIRECT = "REDIRECT"
export const GET_PROFILE = "GET_PROFILE"
export const PROFILE_ERROR= "PROFILE_ERROR"
export const LOADER = "LOADER"
export const LOADER_SPIN = "LOADER_SPIN"
export const UPDATE_PROFILE = "UPDATE_PROFILE"