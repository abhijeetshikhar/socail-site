import axios from 'axios'
import {setAlert} from './alert'

import {GET_PROFILE,PROFILE_ERROR,LOADER, UPDATE_PROFILE} from './type'


//Get current profile

export const getCurrentProfile = () =>async dispatch =>{
	try{
		const res = await axios.get('http://localhost:8080/api/profile/me')
		
		dispatch({
			type:GET_PROFILE,
			payload:res.data
		})
	}
	catch(err){
		dispatch(setAlert("Network error ! Please check your connection"));
		dispatch({
			type:PROFILE_ERROR,
			// payload:{msg:err.response.statusText,status:err.response.status}
		})
	}
}

//Create or update Profile

export const createProfile = (formdata,history,edit=false) => async dispatch =>{
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	}
	try{
		dispatch({
			type: LOADER,
			payload: true
		})
		const res = await axios.post("http://localhost:8080/api/profile",formdata,config);
		dispatch({
			type:GET_PROFILE,
			payload:res.data
		});
		dispatch({
			type: LOADER,
			payload: false
		})
		dispatch(setAlert(edit ? 'Profile Updated' : 'Profile Created', 'success'));
		if(!edit){
			history.push('/dashboard')
		}
	}
	catch(err){
		dispatch({
			type: LOADER,
			payload: false
		});
		if(err.response){
			dispatch(setAlert(err.response.data.error));
			dispatch({
				type: PROFILE_ERROR,
				//payload: { msg: err.response.statusText, status: err.response.status }
			})
		}
		
		else {
			dispatch(setAlert("Network error ! Please check your connection"));
			dispatch({
				type: PROFILE_ERROR
			});
		}

		
	}
}

//Add Experience

export const addExperience = (formdata,history) => async dispatch=>{
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	}
	try {
		dispatch({
			type: LOADER,
			payload: true
		})
		const res = await axios.put("http://localhost:8080/api/profile/experience", formdata, config);
		dispatch({
			type: UPDATE_PROFILE,
			payload: res.data
		});
		dispatch({
			type: LOADER,
			payload: false
		})
		dispatch(setAlert("Experience added", "success", 0, "/"))
		//history.push('/dashboard')
		
	}
	catch (err) {
		dispatch({
			type: LOADER,
			payload: false
		});
		if (err.response) {
			dispatch(setAlert(err.response.data.error));
			dispatch({
				type: PROFILE_ERROR,
				//payload: { msg: err.response.statusText, status: err.response.status }
			})
		}

		else {
			dispatch(setAlert("Network error ! Please check your connection"));
			dispatch({
				type: PROFILE_ERROR
			});
		}


	}
}

//Add Education
export const addEducation = (formdata, history) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	}
	try {
		dispatch({
			type: LOADER,
			payload: true
		})
		const res = await axios.put("http://localhost:8080/api/profile/education", formdata, config);
		dispatch({
			type: UPDATE_PROFILE,
			payload: res.data
		});
		dispatch({
			type: LOADER,
			payload: false
		})
		dispatch(setAlert("Education added"))
		history.push('/dashboard')

	}
	catch (err) {
		dispatch({
			type: LOADER,
			payload: false
		});
		if (err.response) {
			dispatch(setAlert(err.response.data.error));
			dispatch({
				type: PROFILE_ERROR,
				//payload: { msg: err.response.statusText, status: err.response.status }
			})
		}

		else {
			dispatch(setAlert("Network error ! Please check your connection"));
			dispatch({
				type: PROFILE_ERROR
			});
		}


	}
}