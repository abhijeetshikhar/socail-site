import React, { useEffect, Fragment } from 'react'
import { connect } from 'react-redux'
import { getCurrentProfile } from '../redux/actions/profile'
import Spinner from '../common/Spinner'
import { Link } from 'react-router-dom'
import DashboardAction from './DashboardAction'
const Dashboard = ({ getCurrentProfile, auth: { user }, profile: { profile, loading } }) => {

	useEffect(() => {
		getCurrentProfile()
	}, [])
	return loading && profile === null ? (
		<Spinner />
	) : (
			<Fragment>
				<h1 className='large text-primary text-center'>Dashboard</h1>
				{/* <p className='lead'>
					<i className='fas fa-user' /> Welcome {user && user.name}
				</p> */}
				{profile !== null ? (
					<Fragment>
						<DashboardAction />
						

						<div className='my-2'>
							{/* <button className='btn btn-danger'>
								<i className='fas fa-user-minus' /> Delete My Account
           					 </button> */}
						</div>
					</Fragment>
				) : (
						<Fragment>
							<p>You have not yet setup a profile, please add some info</p>
							<Link to='/create-profile' className='btn btn-primary my-1'>
								Create Profile
          </Link>
						</Fragment>
					)}
			</Fragment>
	)
}

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile
})

export default connect(mapStateToProps, { getCurrentProfile })(Dashboard)
