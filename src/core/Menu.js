import React , {Fragment }from "react";
import { Link, withRouter } from "react-router-dom";
import {connect} from 'react-redux' 
import {logout} from '../redux/actions/auth'


const Menu = ({ auth: { isAuthenticated,loading,user},logout}) => {
	const authLinks = (
		<ul className="navbar-nav ml-auto">
			{/* <li className="nav-item">
				<Link className="nav-link" to="/dashboard">
					Dashboard
				</Link>
			</li> */}
			<li className="nav-item">
				<Link className="nav-link">
					Welcome {user && user.name}
				</Link>
			</li>
			<li className="nav-item">
				<Link className="nav-link"  onClick={logout} to="/">
					Logout
				</Link>
			</li>
			
			
		</ul>
		
	);
	const guestLinks = (
		<ul className="navbar-nav ml-auto">
			
			<li className="nav-item">
				<Link className="nav-link" to='/signup'>Register</Link>
			</li>
			<li>
				<Link className="nav-link" to='/signin'>Login</Link>
			</li>
		</ul>
	);
	return (
		<div>
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark ml-auto">
				<Link class="navbar-brand" to="/dashboard">Socail Platform</Link>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div className="collapse navbar-collapse" id="navbarNav">
					{!loading && (
						<Fragment>{isAuthenticated ? authLinks : guestLinks}</Fragment>
					)}
				</div>
				
			</nav>
		</div>
	);
}
const mapStateToProps = state =>({
	auth:state.auth //entire auth state,
	
})

export default connect(mapStateToProps, { logout})(Menu);
