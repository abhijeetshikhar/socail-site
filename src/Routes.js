import React,{useEffect} from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import App from "./App";
import {connect} from 'react-redux'
import Login from './users/Login'
import Register from './users/Register'
import Menu from "./core/Menu";
import Home from "./core/Home";
import Dashboard from "./dashboard/Dashboard";
import PrivateRoutes from "./routing/PrivateRoutes";
import Layout from "./layout/Layout";
import { removeAlert } from './redux/actions/alert'
import ResetPassword from "./users/ResetPassword";
import NewPassword from "./users/NewPassword";
import CreateProfile from "./components/ProfileForms/CreateProfile";
import EditProfile from "./components/EditProfile";
import AddExperience from "./components/ProfileForms/AddExperience";
const Routes = (props) => {
  useEffect(() => {
    props.removeAlert()
    

  }, [])
  return (
    <BrowserRouter>
    
      <Switch>
        <Layout>
        <Route path='/' exact component={Home} />
        <Route path='/signup' exact   component={Register} />
        <Route path='/signin' exact component={Login}/>
        <Route path="/forgotpassword" exact component={ResetPassword}/>
        <Route path="/reset/:token" exact component={NewPassword}/>
        <PrivateRoutes path="/dashboard" exact component={Dashboard} />
        <PrivateRoutes path="/create-profile" exact component={CreateProfile}/>
        <PrivateRoutes path="/edit-profile" exact component={EditProfile}/>
        <PrivateRoutes path="/add-experience" exact component={AddExperience} />
        </Layout>
      </Switch>
    </BrowserRouter>    
  );
};

export default connect(null, { removeAlert})(Routes)
